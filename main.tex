%! TeX program = lualatex
\documentclass[
        egregdoesnotlikesansseriftitles,
        paper=a4,
        twocolumn=true,
        fontsize=10pt,
        DIV=calc
]{scrarticle}

\usepackage{pgfplots}
\usepgfplotslibrary{groupplots,dateplot}
\usetikzlibrary{patterns,shapes.arrows}
\pgfplotsset{compat=newest}
\pgfplotsset{every tick label/.append style={font=\small}}
\pgfplotsset{every axis label/.append style={font=\small}}

\usepackage{articlepreamble-spanish}

\usepackage{circuitikz}
\tikzstyle{icdev}=[draw, text width=6em, minimum height=4em]

% Document information
\author{
        Juan David Salcedo Hernández\texorpdfstring{\\
        {\large\href{mailto:jdavid.salcedo@udea.edu.co}{jdavid.salcedo@udea.edu.co}}}{}
        \and
        Natali Andre Julio Beltrán\texorpdfstring{\\
        {\large\href{mailto:natali.julio@udea.edu.co}{natali.julio@udea.edu.co}}}{}
}


\title{Caracterización del perfil de intensidad de un láser mediante el uso de
un motor de pasos}

\begin{document}
\pgfkeys{/pgf/number format/.cd,1000 sep={\,}}
\twocolumn[
\begin{abstract}
        En este informe es una adición al proceso de caracterización de la
        resolución angular de un servomotor. Se presenta la descripción de un
        proceso de identificación del ancho del perfil de intensidad para un
        láser cuya lente de focalización fue removida.
\end{abstract}
]

\section{Introducción}
Se pretende hallar el ángulo de un cono de luz producido por un láser sin lente
unido a un servomotor mediante la utilización de la equivalencia de los pasos y
los grados del experimento anterior e implementando un circuito que utiliza una
fotorresistencia, que es un componente electrónico cuya resistencia disminuye
con el aumento de intensidad de luz incidente.

\section{Consideraciones experimentales}
El ángulo de apertura de un haz de luz se define usualmente como el ancho de la
curva de intensidad en un valor del \SI{50}{\%} de la máxima
luminosidad\footnote{Véase:
        \href{https://doi.org/10.25039/TN.010.2019}{10.25039/TN.010.2019}
}.
En consideración de esto, se procedió a tomar un perfil de intensidad del cono
de luz siguiendo los lineamientos que se describen en secciones subsecuentes.
Si la dispersión del cono de luz es uniforme, entonces se espera este perfil
corresponda a una curva Gaussiana.

\section{Monatje experimental}
Para realizar las mediciones correspondientes a la intensidad del cono de luz con
la fotorresistencia, se tuvieron que implementar dos circuitos conectados a la
tarjeta Arduino ChipKit-UNO32; a saber, el controlador de pasos del servomotor
que se describió en el informe anterior, junto con un circuito de
lectura sobre el voltaje de la fotorresistencia,~\autoref{fig:2}. El proceso de medición fue
automatizado con el uso de un programa en Python, con el cual se registraron los
valores de intensidad en función del paso del motor en unidades arbitrarias.
\begin{figure}[htpb]
        \centering
        \input{pgfplots/circuit.tex}
        \caption{Circuito para la detección de la intensidad por medio de la
        fotorresistencia. A0 es el pin de la placa Arduino.}
        \label{fig:2}
\end{figure}

\subsection{Metodología}
En el eje del servomotor se fijó un láser sin su lente de focalización, de
manera que la luz se propagara de forma cónica. La fotorresistencia se ubicó a
una altura aproximadamente igual a la del láser para medir el perfil de su
intensidad en una línea central.

Se registraron tres series de datos provenientes de este proceso, sobre las cuales
se realizó un ajuste Gaussiano que permitiera establecer el ancho de la curva en
la mitad de la máxima intensidad.

\section{Discusión}
Se escribió un código en Python para ajustar los datos experimentales a una
función de la forma
\begin{equation}
        f(x)=A e^{-(x-\mu)^2/(2\sigma^2)},
\end{equation}
donde $A, \mu, \sigma$ son parámetros libres, y $\mu$ es el valor medio y
$\sigma$ es la desviación estándar.

El parámetro de interés para encontrar el ancho de la curva de intensidad es
$\sigma$, puesto que es posible mostrar que ese valor en la mitad del valor
máximo de la curva está dado por
\begin{equation}\label{eq:1}
        W = 2\sqrt{2 \log 2} \cdot \sigma. 
\end{equation}

Con lo anterior, y con los parámetros encontrados por regresión, se dedujeron
los resultados.

\section{Resultados}
\begin{figure*}[t!]
        \centering
        {\phantomsubcaption\label{fig:1a}}
        {\phantomsubcaption\label{fig:1b}}
        {\phantomsubcaption\label{fig:1c}}
        \tikz\node[
        inner sep=1pt,
        label={[label distance=1mm]120:\subref{fig:1a}}
        ] {\input{./pgfplots/gaussian1.tex}};
        \tikz\node[
        inner sep=1pt,
        label={[label distance=1mm]120:\subref{fig:1b}}
        ] {\input{./pgfplots/gaussian2.tex}};
        \tikz\node[
        inner sep=1pt,
        label={[label distance=1mm]120:\subref{fig:1c}}
        ] {\input{./pgfplots/gaussian3.tex}};
        \caption{Resultados de la regresión Gaussiana para cada uno de los casos
        estudiados. Los valores $\what{A}, \what{\mu}, \what{\sigma}$ son
estimativos de los parámetros de regresión.}\label{fig:1}
\end{figure*}
Para cada una de las series de datos obtenidas, el programa en Python determinó
los parámetros de la curva Gaussiana. Las gráficas de estos ajustes se observan
en la~\autoref{fig:1}. Mediante la~\autoref{eq:1}, se encontró que las anchuras
fueron las siguientes (con un número arbitrario de cifras decimales):
\begin{align}
        W_1 &= \SI{23.2559}{pasos}, \\
        W_2 &= \SI{24.9900}{pasos}, \\
        W_3 &= \SI{24.7341}{pasos}.
\end{align}
En promedio, se tiene que $\wbar{W} = \SI{24.3267}{pasos}$. En consideración de
que los pasos son números enteros, se reporta un valor de \SI{24}{pasos}.
Adicionalmente, dada la correspondencia entre pasos y ángulos que se encontró en
el informe anterior, se tiene que el ángulo del cono de luz es de
aproximadamente \SI{24}{\degree}.

\end{document}
