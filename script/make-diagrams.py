#!/usr/bin/env python3

import regression
import pandas as pd
import numpy as np

# datasets
x_1 = np.arange(60)
intensity_1 = np.array([36, 36, 36, 36, 36, 37, 39, 41, 43, 49, 53, 62, 69, 75, 80, 84, 86, 88,
                        88, 87, 85, 82, 76, 72, 66, 62, 59, 56, 52, 48, 42, 27, 24, 22, 20, 16,
                        14, 11,  6,  5,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
                        0,  0,  0,  0,  0,  0,])
x_2 = np.arange(40)
intensity_2 = np.array([24, 23, 23, 23, 24, 24, 25, 28, 29, 32, 30, 41, 45, 49, 52, 54, 48, 56,
                        56, 56, 54, 52, 48, 46, 41, 39, 37, 35, 33, 31, 27, 23, 20, 19, 17, 14,
                        13,  9,  6,  3,])

x_3 = np.arange(50)
intensity_3 = np.array([24, 24, 24, 24, 24, 25, 26, 28, 29, 32, 35, 41, 45, 45, 51, 54, 55, 55,
                        56, 55, 55, 53, 50, 47, 42, 40, 38, 36, 34, 32, 27, 24, 22, 20, 17, 15,
                        13,  9,  6,  3,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,])

x = np.array([x_1, x_2, x_3], dtype=object)
intensities = np.array([intensity_1, intensity_2, intensity_3], dtype=object)

# names array
names = ['../pgfplots/gaussian1', '../pgfplots/gaussian2', '../pgfplots/gaussian3']

for i, intensity in enumerate(intensities):
    fig = regression.GaussianRegression(x[i], intensity, None,
                                        tags=['Paso', 'Intensidad\,/\,AU'])
    fig.scatter_plot(names[i])
