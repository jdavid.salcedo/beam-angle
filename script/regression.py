#!/usr/bin/env python3

import numpy as np
from scipy import optimize
from scipy import stats
import matplotlib.pyplot as plt
import tikzplotlib

class GaussianRegression:
    def __init__(self, xdata, ydata, yerror, tags=['x','y']):
        self.x = xdata
        self.y = ydata
        self.n = len(xdata)
        self.yerror = yerror
        self.xtag = tags[0]
        self.ytag = tags[1]

    def scatter_plot(self, name, fit=True):
        fig, ax = plt.subplots(1)

        ax.grid(visible=True, which='major', color='#666666', linestyle='--',
                alpha=0.3)
        ax.set(xlabel=r'{}'.format(self.xtag), ylabel=r'{}'.format(self.ytag))
        ax.errorbar(self.x, self.y, yerr=self.yerror, fmt='ok', markersize=1,
                    capsize=2)

        if not fit:
            plt.show()
        elif fit:
            optimal_params, cov_matrix = optimize.curve_fit(
                # Gaussian curve
                lambda i, A, mu, sigma: A*np.exp(-(i-mu)**2/(2*sigma**2)),
                self.x, self.y, sigma=self.yerror)

            A, mu, sigma = optimal_params
            delta_A, delta_mu, delta_sigma = np.sqrt(np.diag(cov_matrix))
            predicted_y = A*np.exp(-(self.x-mu)**2/(2*sigma**2))

            ax.set(title=r'\shortstack{{$\what{{A}} = {}$, $\what{{\mu}} = {}$\\ $\what{{\sigma}} = {}$}}'.format(
                '%.4f' % A, '%.4f' % mu, '%.4f' % sigma))
            ax.plot(self.x, predicted_y, 'k-')
            tikzplotlib.save(f'../pgfplots/{name}'\
                             '.tex', axis_height='6.5cm', axis_width='6.5cm')
