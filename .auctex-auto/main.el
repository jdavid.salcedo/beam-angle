(TeX-add-style-hook
 "main"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("scrarticle" "egregdoesnotlikesansseriftitles" "paper=a4" "twocolumn=true" "fontsize=10pt" "DIV=calc")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "./pgfplots/gaussian1"
    "./pgfplots/gaussian2"
    "./pgfplots/gaussian3"
    "scrarticle"
    "scrarticle10"
    "pgfplots"
    "articlepreamble-spanish"
    "circuitikz")
   (LaTeX-add-labels
    "fig:1a"
    "fig:1b"
    "fig:1c"
    "fig:2"))
 :latex)

